![Bild 1](media/1.jpeg)

**Genom att arbeta med BIM blev produktionstakten överlag snabbare än vanligt och prognoserna säkrare.**

# BIM i hela bygprocessen ger effektivitet och kvalité

> ##### Trafikverket prövar BIM i allt fler projekt. I samband med ett mindre järnvägsprojekt har byggnadsinformationsmodeller präglat arbetet och gett positiva reaktioner från inblandade parter. Projektet visar på många framgångsfaktorer.

DET PÅGÅR ETT OMFATTANDE FÖRÄNDRINGSARBETE av Nynäsbanan mellan Västerhaninge och Nynäshamn. I en första etapp anlades nya mötesspår och stationernas plattformar byggdes ut. För närvarande byggs sträckan Västerhaninge–Tungelsta, en sträcka på 3,5 km, om till planskilt dubbelspår.
​	– Här har vi fullt ut jobbat med anläggningsmodeller och ställt krav på projektörer och entreprenörer att arbeta med BIM, berättar Fredrik Sylvan på Sundsvalls Mätcenter. 
​	Han har lång erfarenhet av att arbeta för Trafikverket och ingår i projektledningsorganisationen för detta projekt, där han ansvarar för mätning och mängdreglering samt BIM-arbete. 
​	I projektet handlades projektören upp utan krav på BIM, vilket berodde på osäkerhet om kravställningen. Men under tiden som förfrågan var ute rätades osäkerheten ut och den upphandlade projektören ställde sig positiv till att arbeta med BIM.
​	– Vi var överens om att modellen ska vara ritningarnas ursprung – endast ritningar och dokument som kom från modellen fick produceras.
​	Fredrik Sylvan ser arbete med BIM som en mognadsprocess. Därför är det viktigt att sprida mycket information och hjälpa alla som är nybörjare. Man kan inte sätta sig på höga hästar och det är viktigt att ha högt i tak.
​	– När vi pratade BIM med andra inom Trafikverket så var fokus ofta på kollisionskontroll, tekniksamordning och samgranskning medan få pratade produktion, till exempel vad grävmaskinisten behöver för underlag. För oss var det viktigt att klargöra för projektörerna att vi även måste ha fokus på slutanvändaren. Våra modeller ska kunna användas direkt i produktionen utan handpåläggning, säger Fredrik Sylvan. 
​	När entreprenör upphandlades var Trafikverket noga med att ange förutsättningarna med BIM – att Trafikverket tillhandahåller modeller och tar ansvar för dem samt att modellerna juridiskt likställs med ritningar (12-handlingar) och är kopplad till AMA-koder. Eftersom entreprenören inte behövde göra
modeller själv var förhoppningen att detta kunde påverka priset
positivt för Trafikverket.
​	För att möjliggöra för alla intresserade att tillgodogöra sig förfrågningsunderlaget och lämna anbud, skickades modellen ut som en 3D-pdf. Dessutom bifogades en videosnutt som förklarade de enklaste verktygen för att kunna hantera denna pdf.
​	I förfrågningsunderlaget framgick även att Trafikverket tillhandahåller ett satellitpositioneringssystem, som alla upphandlade entreprenörer har fri tillgång till, för att utifrån det kunna positionera sina maskiner. Genom detta blev upphandlingen mer konkurrensneutral. Det fanns även här en förhoppning om att detta skulle påverka priset positivt.
​	Att skicka ut modellen i ett allmänt känt format var ett lyckokast – alla kunde titta på den. När anbudstiden löpte ut våren 2011 fanns tre anbudsgivare och av dessa fick NCC kontraktet.
​	Efter att NCC tilldelats uppdraget ägde ett anläggningsmodellmöte rum med projektör och entreprenör, för att klargöra om Trafikverket ytterligare kunde underlätta för entreprenören att hantera modellernas leveransformat.

![Bild 2](media/2.jpeg)

**Modellerna har juridiskt likställts med ritningar och är kopplade till AMA-koder.** 

​	– En kritisk faktor i produktionen är vilket system som används för att navigera maskinerna. Därför är det viktigt att modeller finns i rätt format så vi gjorde en anpassning av leveransformatet utifrån uttryckta önskemål. 
​	Projektet var, på grund av överklagande som krävt två anbudsperioder, cirka ett halvår försenat när entreprenaden väl drog igång i juni. En stor höstavstängning av banan var planerad sedan länge vilket innebar att NCC fick väldigt knappt med tid.
​	– NCC konstaterade sedan att man aldrig hade klarat av att genomföra de schaktarbeten som skulle göras inom avstängningsperioden  om det inte funnits färdiga modeller. Tack vare BIM lyckades vi alltså ro det hela i hamn, säger en nöjd Fredrik Sylvan.

EN SUMMERING AV ERFARENHETERNA visar att produktionen påverkades positivt av BIM. Till exempel krävdes mindre utsättning av markeringskäppar och därmed färre utsättare. Dock behövdes lika många mätingenjörer som bland annat hjälpte maskinister att ladda in programvaror och modeller i maskinerna. Åtskilliga inställningar och kontroller krävs.
​	Överlag blev produktionstakten snabbare än vanligt och prognoserna säkrare, eftersom allt fanns klart och tydligt i modellen. Ett test, där volymer togs fram både med ”gammal” projekteringsmetodik och med hjälp av modellen, visade att mängderna skiljde sig mycket åt.
​	– Beroende på omständigheterna kan mängdberäkningarna vara mycket missvisande vid manuell uträkning. Konsekvensen blir att man i tidiga skeden arbetar med fel volymer vilket kan ge missvisande kostnad och tidsplan. Byggsektorn
får mycket kritik för att inte hålla budget och tider. Här kan vi, med hjälp av BIM, bli mycket bättre.

HUR HAR DÅ REAKTIONERNA VARIT PÅ BIM-ARBETE? Projektörerna tycker att det är bra med beställning där modeller är ett uttalat krav från början. Samtidigt blir arbetet med BIM mer krävande. Å andra sidan är det bra att få en knuff i ryggen för att börja arbeta mer med BIM.
​	Anbudsgivare/entreprenörer uppskattade att modellen är med i förfrågningsunderlaget, att Trafikverket tar ansvar för den och att den är en 12-handling. Att modellen är byggbar och möjlig att överföra i maskin uppskattas också. BIM anses ge en effektivare byggprocess och vara en förutsättning för att klara en kort byggtid.
​	– Vi måste vara behjälpliga gentemot både projektörer och entreprenörer, säger Fredrik Sylvan. Vi har ett gemensamt ansvar för att komma framåt.

I VINTER SKA DET FINNAS TÅGTRAFIK på båda spåren och projektet beräknas vara avslutat under sommaren 2013. En ny etapp med dubbelspår är på gång och Fredrik Sylvan och hans kollegor arbetar nu med järnvägsplan och systemhandling för kommande etapper. BIM finns nu som krav i upphandling av projektörer. Det är även ett krav att arbeta modellorienterat i systemhandlingsprojekteringen. Erfarenheter av detta, förhållandevis lilla, projekt får således konsekvenser för kommande projekt. 